export class Post {

  title: string;
  content: string;
  loveCount: number;
  createdAt: Date;

  constructor(title: string, content: string, loveCount: number, createdAt: Date) {
    this.title = title;
    this.content = content;
    this.loveCount = loveCount;
    this.createdAt = createdAt;
  }

}
