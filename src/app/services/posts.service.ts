import { Injectable } from '@angular/core';
import { Post } from 'src/app/models/post';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  posts: Post[] = [
    new Post('Premier Post', 'Contenu de mon premier post...', 0, new Date()),
    new Post('Deuxième Post', 'Contenu de mon deuxième post...', 0, new Date()),
    new Post('Troisième Post', 'Contenu de mon troisième post... Youpie', 0, new Date())
  ];
  postsSubject: Subject<Post[]> = new Subject();

  constructor() { }

  emitPosts() {
    this.postsSubject.next(this.posts);
  }

  addPost(post: Post) {
    this.posts.push(post);
    this.emitPosts();
  }

  deletePost(position: number) {
    this.posts.splice(position, 1);
    this.emitPosts();
  }

  increaseLoveCount(postPosition: number) {
    this.posts[postPosition].loveCount += 1;
    this.emitPosts();
  }

  decreaseLoveCount(postPosition: number) {
    this.posts[postPosition].loveCount -= 1;
    this.emitPosts();
  }

}
