import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/models/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() post: Post;
  @Input() position: number;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }

  onLoveItClicked() {
    this.postsService.increaseLoveCount(this.position);
  }

  onDontLoveItClicked() {
    this.postsService.decreaseLoveCount(this.position);
  }

  getPostTitleColor() {
    if (this.post.loveCount > 0) {
      return 'green';
    } else if (this.post.loveCount < 0) {
      return 'red';
    } else {
      return 'black';
    }
  }

  onDeletePost() {
    this.postsService.deletePost(this.position);
  }

}
